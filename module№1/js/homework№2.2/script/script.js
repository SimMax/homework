let m = +prompt("Введите значение m (целое число больше единицы):");
let n = +prompt("Введите значение n (целое число больше первого значения и больше единицы):");

while(!Number.isInteger(m) || !Number.isInteger(n) || Number.isNaN(m)|| Number.isNaN(n) || m < 2 || n < 2 || m >= n){
    let flag = confirm("Некорректно введенные значения. Начать заново?")
    if (flag){
        m = +prompt("Введите значение m (целое число больше единицы):");
        n = +prompt("Введите значение n (целое число больше первого значения и больше единицы):");
    }
}

for(let i = m; i <= n; i++){
    if(isSimple(i)){
        document.write(i + " ");
    }
}
function isSimple(num){
    let flag = true;
    for(let i = 2; i < num; i++){
        if(num % i == 0){
            flag = false;
            break;
        }
    }
    return flag;
}

