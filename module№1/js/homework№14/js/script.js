'use strict'
$('.display input').val('0');
let first = 0;
let second = 0;
let memory = 0;
let sign;
let result;
let value;
let flagPoint = true;
let indexNumber = true;
let flagAction = true;
let flagMemory = true;

$(document).keypress(function(event){
    value = event.which;
    switch (value){
        case 48:{
            value = '0';
            action(value);
            break;
        }
        case 49:{
            value = '1';
            action(value);
            break;
        }
        case 50:{
            value = '2';
            action(value);
            break;
        }
        case 51:{
            value = '3';
            action(value);
            break;
        }
        case 52:{
            value = '4';
            action(value);
            break;
        }
        case 53:{
            value = '5';
            action(value);
            break;
        }
        case 54:{
            value = '6';
            action(value);
            break;
        }
        case 55:{
            value = '7';
            action(value);
            break;
        }
        case 56:{
            value = '8';
            action(value);
            break;
        }
        case 57:{
            value = '9';
            action(value);
            break;
        }
        case 43:{
            value = '+';
            action(value);
            break;
        }
        case 45:{
            value = '-';
            action(value);
            break;
        }
        case 42:{
            value = '*';
            action(value);
            break;
        }
        case 47:{
            value = '/';
            action(value);
            break;
        }
        case 46:{
            value = '.';
            action(value);
            break;
        }
        //Enter
        case 13:{
            value = '=';
            action(value);
            break;
        }
        //C
        case 99:{ 
            value = 'C';
            action(value);
            break;
        }
        //Shift+P
        case 80:{  
            value = 'm+';
            action(value); 
            break;
        }
        //Shift+Q
        case 81:{ 
            value = 'm-';
            action(value);
            break;
        }
        //Shift+R
        case 82:{ 
            value = 'mrc';
            action(value);
            break;
        }
    }
    // console.log(first)
    // console.log(second)
    // console.log(result)
});

$('.keys').on('click', 'input', function(){
    value = $(this).val();
    action(value);
    // console.log(first)
    // console.log(second)
    // console.log(result)
});

function action(value){  
    if(value == '/' || value == '*' || value == '-' || value == '+'){
        if(indexNumber == false){
            result = calc(parseFloat(first), parseFloat(second), sign);
            $('.display input').val(result);
            first = result;
            second = 0;
        } 
        indexNumber = false;
        flagPoint = true;
        sign = value;
        flagAction = true;
        flagMemory = true;
    }
    if ($.isNumeric(value) &&  flagAction == false ){
        first = 0;
        second = 0;
        flagAction = true;
        flagPoint = true;
        indexNumber = true;
        flagMemory = true;
    }
    if(value == '='){
        if(indexNumber == false){
            result = calc(parseFloat(first), parseFloat(second), sign);
            $('.display input').val(result);
            first = result;
            second = 0;
            indexNumber = true;
            flagAction = false;
            flagMemory = true;
        } 
    }
    if(value == 'C'){
        $('.display input').val('0');
        first = 0;
        second = 0;
        flagPoint = true;
        indexNumber = true;
        flagMemory = true;
    }
    if(value == 'mrc'){
        if(flagMemory == false){
            memory = 0;
            $('#memory').hide();
        }
        $('.display input').val(memory);
        second = 0;
        first = memory;
        indexNumber = true;
        flagAction = false;
        flagMemory = false;
    }
    if(value == 'm+' || value == 'm-'){
        if(value == 'm+'){
            memory = memory + parseFloat($('.display input').val());
        } else if(value == 'm-'){
            memory = memory - parseFloat($('.display input').val());
        }
        $('#memory').css({'display': 'block'});
        second = 0;
        indexNumber = true;
        flagAction = false;
        flagMemory = true;
    }
    if(indexNumber){
        first = inputNumber(value, first);
    } else {
        second = inputNumber(value, second);
    }
}

function inputNumber(value, number){
    if($.isNumeric(value) || value == '.'){
        if(String(number).indexOf('.') > -1 && value == '.'){
            //
        } else {
            number += String(value); 
        }
        if(value == '.'){
            if(flagPoint){
                $('.display input').val(parseFloat(number) + '.');
                flagPoint = false;
            }  
        } else if(flagPoint == false && value == '0'){
            $('.display input').val(($('.display input').val() + '0'))   
        } else {
            $('.display input').val(parseFloat(number));
        }  
    }
    return number;
}
function calc(first, second, sign){
    switch(sign){
        case '+':{
            return first + second;
        }
        case '-':{
            return first - second;
        }
        case '*':{
            return first * second;
        }
        case '/':{
            return first / second;
        }
    }
}