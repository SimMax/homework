let obj = {
    name: 'Name',
    surname: 'Surname',
    age: 33,
    address: ['city', 'street', 'house', 34],
    parent: {
        parentName: 'parent name',
        parentSurname: 'parent surname',
        parentAge: 66, 
        parentAddress: ['city', 'street', 'house', 55, 999],
        own: {
            car: 'skoda',
            house: 'address',
            money: 9999999
        }
    },
    brother: {
        name: 'Name',
        surname: 'Surname',
        age: 35
    }
};

function cloneFunction(obj){
    let newObj = {};
    for(let i in obj){
        if(Array.isArray(obj[i])){
            let mas = [];
            for(let j = 0; j < obj[i].length; j++){
                mas.push(obj[i][j]);
            }
            newObj[i] = mas;
        }else if(typeof(obj[i]) == 'object'){
            newObj[i] = cloneFunction(obj[i]);
        }else{
            newObj[i] = obj[i];
        }
    }
    return newObj;
}

console.log(obj);
console.log(cloneFunction(obj));