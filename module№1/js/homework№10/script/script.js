'use strict'
function counting(users, tasks, deadline){
    let today = new Date();

    let teamProductivity = 0;
    for(let i = 0; i < users.length; i++){
        teamProductivity += users[i];
    }

    let storyPoints = 0;
    for(let i = 0; i < tasks.length; i++){
        storyPoints += tasks[i];
    }
    let projectTime = storyPoints/teamProductivity;

    let weekendDays = calculateWeekendDays(today, new Date(today.getFullYear(),today.getMonth(),today.getDate() + projectTime));

    let finalDate = new Date(today.getFullYear(),today.getMonth(),today.getDate() + weekendDays);
    
    console.log('finalDate: ' + finalDate);
    console.log('deadline: ' + deadline);

    if(finalDate <= deadline){
        return `Все задачи будут успешно выполнены за ${(deadline - finalDate)/(1000*60*60*24)} дней до наступления дедлайна!`;
    } else {
        return `Команде разработчиков придется потратить дополнительно ${(finalDate - deadline)*8/(1000*60*60*24)} часов после дедлайна, чтобы выполнить все задачи в беклоге`;
    }

}

function calculateWeekendDays(fromDate, toDate){
    let weekendDayCount = 0;
    while(fromDate < toDate){
        fromDate.setDate(fromDate.getDate() + 1);
        if(fromDate.getDay() === 0 || fromDate.getDay() === 6){
            ++weekendDayCount;
        }
    }
    return weekendDayCount;
}

console.log(counting([2,6,3,4,2], [4,2,18,45,180,75], new Date(2018,11,29)));
//console.log(counting([2,6,3,4,2], [4,2,18,45,180,75], new Date(2019,0,22)));
