const num1 = 1;
const num2 = 1;

let n = +prompt("Введите порядковый номер числа Фибоначчи:");

while(!Number.isInteger(n) || Number.isNaN(n)){
    let flag = confirm("Некорректные значения. Продолжить?");
    if (flag){
        n = +prompt("Введите порядковый номер числа Фибоначчи:")
    }
}

document.write(n + " значение числа Фибоначчи: " + fib(n));

function fib(n){
    let queue = [];
    queue.push(num1);
    queue.push(num2);
    if(n===1 || n===2){
        return queue[n-1];
    } else if (n===0){
        return 0;
    } else if(n > 2){
        for(let i = 0; i < n-2; i++){
            queue.push(queue[0] + queue[1]);
            queue.shift();
        }
        return queue[1];
    } else {
        for(let i = 0; i < -n-2; i++){
            queue.push(queue[0] + queue[1]);
            queue.shift();
        }
        return Math.pow(-1,-n+1)*queue[1];
    }
}