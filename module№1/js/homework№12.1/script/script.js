'use strict'
let button = document.getElementById('btn');
button.addEventListener('click', ()=>{
    button.parentNode.removeChild(button);

    let diameter = document.createElement('input');
    diameter.id = 'diametr_of_circle';
    diameter.placeholder = 'Диаметр круга';
    diameter.style.marginRight = '20px';
    document.body.appendChild(diameter);

    let color = document.createElement('input');
    color.id = 'color_of_circle';
    color.placeholder = 'Цвет круга';
    document.body.appendChild(color);

    let button_paint = document.createElement('input');
    button_paint.type = 'button';
    button_paint.value = 'Нарисовать';
    button_paint.style.marginTop = '10px';
    button_paint.style.display = 'block';
    document.body.appendChild(button_paint);

    button_paint.addEventListener('click', ()=>{
        let circle = document.createElement('div');
        let d = document.getElementById('diametr_of_circle').value;
        let c = document.getElementById('color_of_circle').value;
        circle.style = `border-radius: 50%; margin-top: 10px; background: ${c}; width: ${d}px; height:${d}px`;
        document.body.appendChild(circle);
    });
});