'user strict'

function createNewUser(){
    let user = {};
    let fstName = prompt("Введите имя:");
    let lstName = prompt("Введите Фамилию:");

    Object.defineProperties(user, {
        'firstName': {
          value: fstName,
          configurable: true
        },
        'lastName': {
          value: lstName,
          configurable: true
        },
        getLogin: {
            get: function(){
                return (this.firstName.split('')[0] + this.lastName).toLowerCase();
            }
        },
        setFirstName:{
            set: function(newFirstName){
                delete this.firstName;
                Object.defineProperty(user, 'firstName', {
                    value: newFirstName,
                    configurable: true});
            }
        },
        setLastName: {
            set: function(newLastName){
                delete this.lastName;
                Object.defineProperty(user, 'lastName', {
                    value: newLastName,
                    configurable: true});
            }
        }
    });
    console.log(user);
    console.log(user.getLogin);
    /****************/
    user.firstName = 'f'
    user.setFirstName = 'first';
    user.lastName = 'l'
    user.setLastName = 'last';
    /****************/
    console.log(user);
    console.log(user.getLogin);

    return user;
}

createNewUser();
