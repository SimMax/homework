'use strict'
let button = document.getElementById('btn');
button.addEventListener('click', ()=>{
    button.parentNode.removeChild(button);

    let diameter = document.createElement('input');
    diameter.id = 'diametr_of_circle';
    diameter.placeholder = 'Диаметр круга';
    document.body.appendChild(diameter);

    let button_paint = document.createElement('input');
    button_paint.type = 'button';
    button_paint.value = 'Нарисовать';
    button_paint.style.marginTop = '10px';
    button_paint.style.display = 'block';
    document.body.appendChild(button_paint);

    button_paint.addEventListener('click', ()=>{
        for(let i = 0; i < 100; i++){
            let d = document.getElementById('diametr_of_circle').value;
            let circle = document.createElement('div');
            circle.style = `border-radius: 50%; float: left; margin: 5px; background: rgb(${getRandom()}, ${getRandom()}, ${getRandom()}); width: ${d}px; height:${d}px`;
            if(i%10 == 0){
                circle.style.clear = 'both';
            }
            document.body.appendChild(circle);
            circle.addEventListener('click',()=>{
                circle.parentNode.removeChild(circle);
            });
        }
        function getRandom(){
            return Math.random() * 255;
        }
    });
});