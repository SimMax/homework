let number = +prompt("Введите целое число больше единицы:");

while(!Number.isInteger(number) || Number.isNaN(number) || number < 2){
    number = +prompt("Некорректно введенное число. Введите целое число больше единицы:");
}

for(let i = 2; i <= number; i++){
    if(isSimple(i)){
        document.write(i + " ");
    }
}
function isSimple(num){
    let flag = true;
    for(let i = 2; i < num; i++){
        if(num % i == 0){
            flag = false;
            break;
        }
    }
    return flag;
}

