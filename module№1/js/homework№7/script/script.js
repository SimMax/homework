const users_1 = [{
    name: "Ivan",
    surname: "Ivanov",
    gender: "male",
    age: 30
  },
  {
    name: "Boris",
    surname: "Ivanov",
    gender: "male",
    age: 22
  },
  {
    name: "Maria",
    surname: "Ivanova",
    gender: "female",
    age: 30
  }];
const users_2 = [{
    name: "Ivan",
    surname: "Borisov",
    gender: "male",
    age: 25
  },
  {
    name: "Anna",
    surname: "Ivanova",
    gender: "female",
    age: 24
  },
  {
    name: "Anton",
    surname: "Kirpa",
    gender: "male",
    age: 18
  }];

function excludeBy(list_first, list_second, value){
    let new_list = [];
    for(let i = 0; i < list_first.length; i++){
        let count = 0;
        for(let j = 0; j < list_second.length; j++){
            if(list_first[i][value] === list_second[j][value]){
                count++;
                break;
            } 
        }
        if(count == 0) new_list.push(list_first[i]);
    }
    return new_list;
}
console.log(excludeBy(users_1, users_2, 'name'));
console.log(excludeBy(users_1, users_2, 'surname'));
console.log(excludeBy(users_2, users_1, 'name'));
console.log(excludeBy(users_2, users_1, 'surname'));