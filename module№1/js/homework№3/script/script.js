let num = +prompt("Введите целое число больше 0:");

while(Number.isNaN(num) || !Number.isInteger(num) || num <= 0){
    num = +prompt("Некорректное значение. Введите целое число больше 0:");
}

document.write("Факториал числа " + num + " = " + factorial(num));

function factorial(num){
    let result = 1;
    for(let i = num; i > 0; i--){
        result *= i;
    }
    return result;
}