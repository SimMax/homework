'use strict'
let datePearson = prompt('Введите дату рождения в формате (dd.mm.yyyy):').split('.');

function getAge(datePearson){
    let dateNow = new Date();
    let nowDay = dateNow.getDate();
    let nowMonth = dateNow.getMonth()+1;
    let nowYear = dateNow.getFullYear();

    let pearsonDay = datePearson[0];
    let pearsonMonth = datePearson[1];
    let pearsonYear = datePearson[2];

    let age = nowYear - pearsonYear;

    if((nowMonth < pearsonMonth) || ((nowMonth == pearsonMonth) && nowDay < pearsonDay)){
        age--;
    }
    return age;
}
function getZodiac(datePearson){
    let pearsonDay = datePearson[0];
    let pearsonMonth = datePearson[1];
    switch(pearsonMonth){
        case '01':
            if(pearsonDay > 0 && pearsonDay <= 19){
                return 'Козерог';
            } else if(pearsonDay > 19 && pearsonDay <= 31){
                return 'Водолей';
            } else return 'Вы ввели некорректные данные';
        case '02':
            if(pearsonDay > 0 && pearsonDay <= 18){
                return 'Водолей';
            } else if(pearsonDay > 18 && pearsonDay <= 29){
                return 'Рыбы';
            } else return 'Вы ввели некорректные данные';
        case '03':
            if(pearsonDay > 0 && pearsonDay <= 20){
                return 'Рыбы';
            } else if(pearsonDay > 20 && pearsonDay <= 31){
                return 'Овен';
            } else return 'Вы ввели некорректные данные';
        case '04':
            if(pearsonDay > 0 && pearsonDay <= 19){
                return 'Овен';
            } else if(pearsonDay > 19 && pearsonDay <= 30){
                return 'Телец';
            } else return 'Вы ввели некорректные данные';
        case '05':
            if(pearsonDay > 0 && pearsonDay <= 20){
                return 'Телец';
            } else if(pearsonDay > 20 && pearsonDay <= 31){
                return 'Близнецы';
            } else return 'Вы ввели некорректные данные';
        case '06':
            if(pearsonDay > 0 && pearsonDay <= 20){
                return 'Близнецы';
            } else if(pearsonDay > 20 && pearsonDay <= 30){
                return 'Рак';
            } else return 'Вы ввели некорректные данные';
        case '07':
            if(pearsonDay > 0 && pearsonDay <= 22){
                return 'Рак';
            } else if(pearsonDay > 22 && pearsonDay <= 31){
                return 'Лев';
            } else return 'Вы ввели некорректные данные';
        case '08':
            if(pearsonDay > 0 && pearsonDay <= 22){
                return 'Лев';
            } else if(pearsonDay > 22 && pearsonDay <= 31){
                return 'Дева';
            } else return 'Вы ввели некорректные данные';
        case '09':
            if(pearsonDay > 0 && pearsonDay <= 22){
                return 'Дева';
            } else if(pearsonDay > 22 && pearsonDay <= 30){
                return 'Весы';
            } else return 'Вы ввели некорректные данные';
        case '10':
            if(pearsonDay > 0 && pearsonDay <= 22){
                return 'Весы';
            } else if(pearsonDay > 22 && pearsonDay <= 31){
                return 'Скорпион';
            } else return 'Вы ввели некорректные данные';
        case '11':
            if(pearsonDay > 0 && pearsonDay <= 21){
                return 'Скорпион';
            } else if(pearsonDay > 21 && pearsonDay <= 30){
                return 'Стрелец';
            } else return 'Вы ввели некорректные данные';
        case '12':
            if(pearsonDay > 0 && pearsonDay <= 21){
                return 'Стрелец';
            } else if(pearsonDay > 21 && pearsonDay <= 31){
                return 'Козерог';
            } else return 'Вы ввели некорректные данные';
        default:
            return 'Вы ввели некорректные данные';
    }
}
function getAnimal(datePearson){
    let pearsonYear = datePearson[2];
    let year = (pearsonYear - 1900)%12 + 1;
    switch(year){
        case 1:
            return 'Крыса';
        case 2:
            return 'Бык';
        case 3:
            return 'Тигр';
        case 4:
            return 'Кролик';
        case 5:
            return 'Дракон';
        case 6:
            return 'Змея';
        case 7:
            return 'Лошадь';
        case 8:
            return 'Коза';
        case 9:
            return 'Обезьяна';
        case 10:
            return 'Петух';
        case 11:
            return 'Собака';
        case 12:
            return 'Свинья';
        default:
            return 'Вы ввели некорректные данные';
    }
}

alert(`Вам ${getAge(datePearson)} лет!`);
alert(`Ваш знак зодиака: ${getZodiac(datePearson)}`);
alert(`Год животного по китайскому календарю: ${getAnimal(datePearson)}`);
