'use strict'
let minFirst = document.getElementById('min_first'),
    minSecond = document.getElementById('min_second'),
    secFirst = document.getElementById('sec_first'),
    secSecond = document.getElementById('sec_second'),
    milFirst = document.getElementById('mil_first'),
    milSecond = document.getElementById('mil_second'),
    milThird = document.getElementById('mil_third');

    minFirst.innerHTML = 0;
    minSecond.innerHTML = 0;
    secFirst.innerHTML = 0;
    secSecond.innerHTML = 0;
    milFirst.innerHTML = 0;
    milSecond.innerHTML = 0;
    milThird.innerHTML = 0;

let btnStart = document.getElementById('start');
btnStart.innerText = 'Start';

let btnClear = document.getElementById('clear');
btnClear.innerText = 'Clear';

let pause = 0;

btnStart.addEventListener('click', function(){
    let speed = 50,
        counter = 0,
        startTime = new Date(),
        buttonField = document.getElementById('button_field'),
        btnPause = document.createElement('button');
    btnPause.innerText = 'Pause';
    btnStart.remove();
    buttonField.appendChild(btnPause);

    instance();
    function instance()
    {
        let real = (counter * speed),
            realTime = new Date(),
            ideal = (realTime.getTime() - startTime.getTime() + pause);
        counter++;

        let time = new Date();
        time.setHours(0, 0, 0, 0);
        time.setMilliseconds(ideal);

        minFirst.innerHTML = `${(time.getMinutes() - time.getMinutes()%10)/10}`;
        minSecond.innerHTML = `${time.getMinutes()%10}`;
        secFirst.innerHTML = `${(time.getSeconds() - time.getSeconds()%10)/10}`;
        secSecond.innerHTML = `${time.getSeconds()%10}`;
        milFirst.innerHTML = `${(time.getMilliseconds() - time.getMilliseconds()%100)/100}`;
        milSecond.innerHTML = `${(time.getMilliseconds()%100 - time.getMilliseconds()%10)/10}`;
        milThird.innerHTML = `${time.getMilliseconds()%10}`;
        
        let diff = (ideal - real);

        btnPause.addEventListener('click', function(){
            clearTimeout(stream);
            btnPause.remove();
            buttonField.appendChild(btnStart);
            pause = ideal;
            
        });
        btnClear.addEventListener('click', function(){
            clearTimeout(stream);
            minFirst.innerHTML = 0;
            minSecond.innerHTML = 0;
            secFirst.innerHTML = 0;
            secSecond.innerHTML = 0;
            milFirst.innerHTML = 0;
            milSecond.innerHTML = 0;
            milThird.innerHTML = 0;
            btnPause.remove();
            buttonField.appendChild(btnStart);
            pause = 0;
        });
        let stream = setTimeout(function() { instance(); }, (speed - diff));
    };
});