let count = false;
let info = localStorage.getItem('design');
if (info === 'night'){
    count = !count;
    setNight();
} 

$('#btn').on('click', function(){
    count = !count;
    if(count){
        setNight();
    } else {
        setDay();
    }
});
function setDay(){
    $('.main_block').removeClass('main_block-night');
    $('.logo').removeClass('logo-night');
    $('.logo_color').removeClass('logo_color-night');
    $('.menu_list').removeClass('menu_list-night');
    $('.vertical_menu').removeClass('vertical_menu-night');
    $('.vertical_list').removeClass('vertical_list-night');
    $('.picture').removeClass('picture-night');
    $('.text').removeClass('text-night');
    $('.footer').removeClass('footer-night');
    $('.footer-link').removeClass('footer-link-night');
    localStorage.setItem('design', 'day');
}
function setNight(){
    $('.main_block').addClass('main_block-night');
    $('.logo').addClass('logo-night');
    $('.logo_color').addClass('logo_color-night');
    $('.menu_list').addClass('menu_list-night');
    $('.vertical_menu').addClass('vertical_menu-night');
    $('.vertical_list').addClass('vertical_list-night');
    $('.picture').addClass('picture-night');
    $('.text').addClass('text-night');
    $('.footer').addClass('footer-night');
    $('.footer-link').addClass('footer-link-night');
    localStorage.setItem('design', 'night');
}
