'user strict'
let vehicles = [{name: 'Lorem word_1 dolor sit, amet consectetur adipisicing word_2 elit.'},
    {description: ['Repellendus temporibus.','Word_1 Nisi deleniti.','Veritatis.','Saepe tempore word_2 maiores.']},
    {contentType: {name: ['Word_1 provident qui.','Sed itaque cumque iste saepe veniam quas recusandae.','Repellat temporibus ea dolor facere harum labore.'], 
                description: 'Ipsum, doloremque cupiditate.', 
                locales: 'Consequatur word_2!'}},
    {locales: {name: ['Accusamus quia, enim word_2.'],
                description: {content:'Reprehenderit beatae ratione?'}}}];
               // 'name', 'description', 'contentType.name', 'locales.name','locales.description.content'
let result = [];

function filterCollection(mas, key, flag, ...fieldNames){
    let obj = {};
    for(let i = 0; i < mas.length; i++){
        for(let j = 0; j < fieldNames.length; j++){
            let field = fieldNames[j].split('.');
            obj = mas[i];
            for(let k = 0; k < field.length; k++){
                if(typeof obj !== 'undefined'){
                    obj = obj[field[k]];
                } 
            }
            if(typeof obj !== 'undefined'){
                filterText(obj, key, flag);
            } 
        }
    }
}
function filterText(obj, key, flag){
    let keys = key.split(' ');
    let count = 0;
    if(flag){
        if(Array.isArray(obj)){
            for(let j = 0; j < keys.length; j++){
                for(let i = 0; i < obj.length; i++){
                    if(obj[i].toLowerCase().indexOf(keys[j].toLowerCase()) >= 0){
                        count++;
                    }
                }
            }
            if(count === keys.length){
                result.push(obj);
            }
        } else {
            for(let j = 0; j < keys.length; j++){
                if(obj.toLowerCase().indexOf(keys[j].toLowerCase()) >= 0){
                    count++;
                }
            }
            if(count === keys.length){
                result.push(obj);
            }
        }
    } else {
        if(Array.isArray(obj)){
            for(let j = 0; j < keys.length; j++){
                for(let i = 0; i < obj.length; i++){
                    if(obj[i].toLowerCase().indexOf(keys[j].toLowerCase()) >= 0){
                        count++;
                        break;
                    }
                }
                if(count === 1) {
                    result.push(obj);
                    break;
                }
            }
           
        } else {
            for(let j = 0; j < keys.length; j++){
                if(obj.toLowerCase().indexOf(keys[j].toLowerCase()) >= 0){
                    result.push(obj);
                    break;
                }
            }
        }
    }
}

//filterCollection(vehicles, 'word_1 word_2', false, 'name', 'description', 'contentType.name', 'locales.name','locales.description.content');
filterCollection(vehicles, 'word_1 word_2', true, 'name', 'description', 'contentType.name', 'locales.name','locales.description.content');
console.log(result);

