'use strict'

let count = prompt('Введите число пунктов, которые будут в списке:');
let listMap = new Map();

for(let i = 1; i <= count; i++){
    listMap.set(i, prompt(`Введите содержимое пункта ${i}:`));
}

for (let [key, value] of listMap) {
    let list = document.createElement('p');
    list.innerText = `${key}: ${value}`;
    list.style.margin = '8px';
    document.body.appendChild(list);
    setTimeout(() => {
    list.parentNode.removeChild(list);
    }, 10000);
}
