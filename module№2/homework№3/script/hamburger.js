'use strict'

class Hamburger{ 
    constructor (size, stuffing){
        try { 
            if(!size || !stuffing){
                throw new HamburgerException('no size given');
            } 
            if(size.name != SIZE_SMALL.name && size.name != SIZE_LARGE.name){
                throw new HamburgerException(`invalid size - '${size.name}'`);
            }
            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
        }
        catch(e) {
            console.log(`${e.name}: ${e.message}`);
        }
    }
        
    addTopping(topping){
        try { 
            for(let i = 0; i < this.toppings.length; i++){
                if(this.toppings[i].name == topping.name){
                    throw new HamburgerException(`duplicate topping - '${topping.name}'`);
                } 
            }
            this.toppings.push(topping); 
        } catch(e){
            console.log(`${e.name}: ${e.message}`);
        }
    }

    removeTopping(topping){
        try{
            let flag = true;
            for(let i = 0; i < this.toppings.length; i++){
                if(this.toppings[i] === topping){
                    this.toppings.splice(i, 1);
                    flag = false;
                }
            }
            if(flag) throw new HamburgerException(`doesn't have topping - '${topping.name}'`);
        } catch(e){
            console.log(`${e.name}: ${e.message}`);
        }
        
    }

    getToppings(){
        return this.toppings;
    }

    getSize(){
        return this.size;
    }

    getStuffing(){
        let stuffingHamburger = '';
        for(let i = 0; i < this.toppings.length; i++){
            stuffingHamburger += this.toppings[i].name + '; ';
        }
        return `${this.size.name}; ${this.stuffing.name}; ${stuffingHamburger}`
    }

    calculatePrice(){
        let toppingsPrice = 0;
        for(let i = 0; i < this.toppings.length; i++){
            toppingsPrice += this.toppings[i].price;
        }
        return this.size.price + this.stuffing.price + toppingsPrice;
    }

    calculateCalories(){
        let toppingsCalories = 0;
        for(let i = 0; i < this.toppings.length; i++){
            toppingsCalories += this.toppings[i].calories;
        }
        return this.size.calories + this.stuffing.calories + toppingsCalories;
    }
} 

class HamburgerException extends Error{
    constructor(message){
        super(message);
        this.name = 'HamburgerException';
        this.message = message;
    }
}



