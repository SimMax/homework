/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) { 
    try { 
        if(!size || !stuffing){
            throw new HamburgerException('no size given');
        } 
        if(size.name != Hamburger.SIZE_SMALL.name && size.name != Hamburger.SIZE_LARGE.name){
            throw new HamburgerException(`invalid size '${size.name}'`);
        }
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = [];
    } catch(err){
        console.log(`${err.name}: ${err.message}`);
    }
} 

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {name: 'SIZE_SMALL', price: 50, calories: 20};
Hamburger.SIZE_LARGE = {name: 'SIZE_LARGE', price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: 'STUFFING_CHEESE', price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: 'STUFFING_SALAD', price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: 'STUFFING_POTATO', price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: 'TOPPING_MAYO', price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: 'TOPPING_SPICE', price: 15, calories: 0};

/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function (topping){
    try { 
        for(let i = 0; i < this.toppings.length; i++){
            if(this.toppings[i].name == topping.name){
                throw new HamburgerException(`duplicate topping '${topping.name}'`);
            } 
        }
        this.toppings.push(topping); 
    } catch(err){
        console.log(`${err.name}: ${err.message}`);
    }
}

/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping){
    try{
        let flag = true;
        for(let i = 0; i < this.toppings.length; i++){
            if(this.toppings[i] === topping){
                this.toppings.splice(i, 1);
                flag = false;
            }
        }
        if(flag) throw new HamburgerException(`doesn't have topping - '${topping.name}'`);
    } catch(err){
        console.log(`${err.name}: ${err.message}`);
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function (){
    return this.toppings;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function (){
    return this.size;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
    let stuffingHamburger = '';
    for(let i = 0; i < this.toppings.length; i++){
        stuffingHamburger += this.toppings[i].name + '; ';
    }
    return `${this.size.name}; ${this.stuffing.name}; ${stuffingHamburger}`
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function (){
    let toppingsPrice = 0;
    for(let i = 0; i < this.toppings.length; i++){
        toppingsPrice += this.toppings[i].price;
    }
    return this.size.price + this.stuffing.price + toppingsPrice;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    let toppingsCalories = 0;
    for(let i = 0; i < this.toppings.length; i++){
        toppingsCalories += this.toppings[i].calories;
    }
    return this.size.calories + this.stuffing.calories + toppingsCalories;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message){
    this.name = `HamburgerException`;
    this.message = message;
    // console.log(`HamburgerException: ${message}`);
}
HamburgerException.__proto__ = Error;



