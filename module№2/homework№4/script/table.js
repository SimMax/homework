'use strict'
let amount = 30;
let table = createTable(amount);

function createTable(amount){
    let newTable = document.createElement('table');
    newTable.className = 'table';
    for(let i = 0; i < amount; i++){
        let tr = document.createElement('tr');
        for(let j = 0; j < amount; j++){
            let td = document.createElement('td');
            td.setAttribute('class', 'td');
            tr.appendChild(td);
        }
        newTable.appendChild(tr);
    }
    return newTable;
}

document.body.prepend(table);

table.addEventListener('click', (elem)=>{
    elem.target.classList.toggle('td-black');
    elem.stopPropagation();
});
document.body.addEventListener('click', ()=>{
    let masTd = table.getElementsByClassName('td');
    for (let i = 0; i < masTd.length; i++) {
        masTd[i].classList.toggle('td-black');
    }
});