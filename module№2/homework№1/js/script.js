let btn =  document.querySelector('.fa-bars');
let menu = document.querySelector('.main-menu');
let active = false;
btn.addEventListener('click',()=>{
    active = !active;
    if (active){
        menu.style.display = 'block';
        btn.classList.remove('fa-bars');
        btn.classList.add('fa-times');
    }else{
        menu.style.display = 'none';
        btn.classList.remove('fa-times');
        btn.classList.add('fa-bars');
    }
});